#include <SFML\Network.hpp>
#include <SFML\Graphics.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>

#define MAX_MENSAJES 30
#define CLOSER "w"

std::vector<std::string> aMensajes;
sf::TcpSocket socket;
std::mutex myMutex;
bool forceClose;

void receiveThread() {
	sf::Packet pack;
	while (1) {

		sf::Socket::Status status = socket.receive(pack);

		if (status != sf::Socket::Status::Disconnected) {
			std::string s;
			pack >> s;


			if (s == ">exit" || s == "exit") {
				s = "Chat finalizado";
			} 
			else if (s == CLOSER) {
				forceClose = true;
			}

			if(!forceClose)
				aMensajes.push_back(s);

			pack.clear();
		}
		else
			break;
	}
}

int main()
{	
	forceClose = false;

	std::cout << "Trying to connect..." << std::endl;
	sf::TcpSocket::Status status = socket.connect("127.0.0.1", 50000, sf::seconds(15.0f));

	if (status == sf::TcpSocket::Status::Done) {
		sf::Vector2i screenDimensions(800, 600);

		sf::RenderWindow window;
		window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

		sf::Font font;
		if (!font.loadFromFile("courbd.ttf"))
		{
			std::cout << "Can't load the font file" << std::endl;
		}

		std::string mensaje = ">";

		sf::Text chattingText(mensaje, font, 14);
		chattingText.setFillColor(sf::Color(0, 160, 0));
		chattingText.setStyle(sf::Text::Bold);


		sf::Text text(mensaje, font, 14);
		text.setFillColor(sf::Color(0, 160, 0));
		text.setStyle(sf::Text::Bold);
		text.setPosition(0, 560);

		sf::RectangleShape separator(sf::Vector2f(800, 5));
		separator.setFillColor(sf::Color(200, 200, 200, 255));
		separator.setPosition(0, 550);


		std::thread receiver(&receiveThread);
		receiver.detach();

		//local chat
		while (window.isOpen())
		{
			sf::Event evento;
			while (window.pollEvent(evento))
			{
				switch (evento.type)
				{
				case sf::Event::Closed: {
					sf::Packet pack;
					pack << CLOSER;

					socket.send(pack);///

					window.close();
					socket.disconnect();
				}
					break;
				case sf::Event::KeyPressed:
					if (evento.key.code == sf::Keyboard::Escape)
						window.close();
					else if (evento.key.code == sf::Keyboard::Return)
					{
						aMensajes.push_back(mensaje);

						sf::Packet pack;
						pack << mensaje;

						socket.send(pack);
						if (mensaje == ">exit" || mensaje == "exit") {
							window.close();
							socket.disconnect();
						}
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
						mensaje = ">";
					}
					break;
				case sf::Event::TextEntered:
					if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
						mensaje += (char)evento.text.unicode;
					else if (evento.text.unicode == 8 && mensaje.size() > 0)
						mensaje.erase(mensaje.size() - 1, mensaje.size());
					break;
				}
			}

			if (forceClose) {
				socket.disconnect();
				window.close();
			}

			window.draw(separator);
			for (size_t i = 0; i < aMensajes.size(); i++)
			{
				std::string chatting = aMensajes[i];
				chattingText.setPosition(sf::Vector2f(0, 20 * i));
				chattingText.setString(chatting);
				window.draw(chattingText);
			}
			std::string mensaje_ = mensaje + "_";
			text.setString(mensaje_);
			window.draw(text);


			window.display();
			window.clear();

		}
	}
	
}